//
//  BreakingNewsViewController.m
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "BreakingNewsViewController.h"
#import "Feed.h"
#import "NewsTableViewCell.h"
#import "PresentationNewsViewController.h"
@interface BreakingNewsViewController ()
{
    Feed* feed;
}
@end

@implementation BreakingNewsViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSURL* url = [[NSURL alloc] initWithString:_networkAdress];
    feed = [[Feed alloc] initWithURL:url];
      [feed requestFinished];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initiateData)
                                                 name:@"ParsingComplete"
                                               object:nil];
    //_breakingNewsTable.rowHeight = UITableViewAutomaticDimension;
    _breakingNewsTable.estimatedRowHeight = 10.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return feed.feedPosts.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    [cell addText: [(Post*) [feed.feedPosts objectAtIndex:indexPath.item] title]];
   // cell.backgroundColor =[UIColor redColor];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NewsTableViewCell* cell = [_breakingNewsTable cellForRowAtIndexPath:indexPath];
 
PresentationNewsViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"presentationNewsViewController"];
    
    ctrl.imageLink = [(Post*) [feed.feedPosts objectAtIndex:indexPath.item] imageLink];
    ctrl.descript = [(Post*) [feed.feedPosts objectAtIndex:indexPath.item] descript];
    ctrl.fullNewsLink = [[(Post*) [feed.feedPosts objectAtIndex:indexPath.item] guide] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] ;
    
    [self.navigationController pushViewController:ctrl animated:YES];
    
}

-(void) initiateData
{
    [_breakingNewsTable reloadData];
}
- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
