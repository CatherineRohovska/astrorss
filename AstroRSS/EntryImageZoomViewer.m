//
//  EntryImageZoomViewer.m
//  AstroRSS
//
//  Created by User on 11/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "EntryImageZoomViewer.h"

@implementation EntryImageZoomViewer
{
    UIButton* closeButton;
    UIImageView* imgViewer;
    UIView* contentView;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
     
//        contentView = [[UIView alloc] initWithFrame:self.bounds];
//        contentView.contentMode = UIViewContentModeCenter;
//        
//        [self addSubview:contentView];
        
        imgViewer = [[UIImageView alloc] initWithFrame:self.bounds];
        imgViewer.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:imgViewer];
       
       // self.scrollEnabled = NO;
        self.delegate = self;
        self.minimumZoomScale = 0.5;
        self.maximumZoomScale = 1.0;
        self.contentSize = imgViewer.frame.size;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:1.0f];
        self.bouncesZoom = NO;
        self.bounces = NO;
        //close action on twice tap
    
        UITapGestureRecognizer *twiceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeClick)];
        twiceTap.numberOfTapsRequired = 2;
        [self setUserInteractionEnabled:YES];
        [self addGestureRecognizer:twiceTap];
        
      
        
    }
    return self;
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
        return imgViewer;
    
}
-(void)setImage:(UIImage *)image
{
    float scale= 1;
    if (image.size.height/image.size.width>2) {
        scale = image.size.height/self.frame.size.height;
    }
    else
    {
        scale = image.size.width/self.frame.size.width;
    }
    self.minimumZoomScale = 1.0f/scale;

    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);// image.size;
    imgViewer.contentMode = UIViewContentModeScaleAspectFit;
    imgViewer.frame =frame;
    CGSize size = frame.size;//CGSizeMake(imgViewer.frame.size.width, imgViewer.frame.size.height+yOffset);
    self.contentSize = size;
    [self setContentOffset:self.center];
   // self.contentSize = _image.size;
    imgViewer.image = image;
    //imgViewer.center = CGPointMake(self.bounds.size.width/2.0f, self.bounds.size.height/2.0f);
    imgViewer.backgroundColor = [UIColor redColor];
  
    [self setZoomScale:self.minimumZoomScale animated:NO];
}
-(void) closeClick
{
  

    [self removeFromSuperview];
}

- (void)centerContent {
    CGFloat top = 0, left = 0;
    if (self.contentSize.width < self.bounds.size.width) {
        left = (self.bounds.size.width-self.contentSize.width) * 0.5f;
    }
    if (self.contentSize.height < self.bounds.size.height) {
        top = (self.bounds.size.height-self.contentSize.height) * 0.5f;
    }
    self.contentInset = UIEdgeInsetsMake(top, left, top, left);
}

- (void)didAddSubview:(UIView *)subview {
    [super didAddSubview:subview];
    [self centerContent];
}

- (void)scrollViewDidZoom:(__unused UIScrollView *)scrollView {
    [self centerContent];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self centerContent];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self centerContent];
}
@end
