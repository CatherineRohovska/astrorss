//
//  DownloadOperation.h
//  New
//
//  Created by User on 10/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface DownloadOperation : NSOperation
@property(assign) UIImageView* imgView;
@property(strong) NSString* urlPath;
@property(strong) UIActivityIndicatorView* activityIndicator;
@end
