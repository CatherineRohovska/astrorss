//
//  DownloadOperation.m
//  New
//
//  Created by User on 10/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "DownloadOperation.h"
#import "CacheManager.h"
@implementation DownloadOperation
- (void)main {
    // a lengthy operation
    @autoreleasepool {
      CacheManager* manager = [CacheManager sharedManager];
      // UIWebView* contentView = uiweb
        if (!self.isCancelled){
             [_activityIndicator startAnimating];
            NSURL *url = [NSURL URLWithString: _urlPath];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        UIImage *image = [[UIImage alloc] initWithData:data];
            [manager saveToCache:_urlPath image:image];
           
            dispatch_async(dispatch_get_main_queue(), ^{
             
                [_imgView setImage:image];
                [_activityIndicator stopAnimating];
            });
            
        }
        //[newImage release];
        }
 
}
@end
