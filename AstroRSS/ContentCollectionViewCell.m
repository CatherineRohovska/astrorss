//
//  ContentCollectionViewCell.m
//  AstroRSS
//
//  Created by User on 11/10/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ContentCollectionViewCell.h"
#import "DownloadOperation.h"
#import "CacheManager.h"
@implementation ContentCollectionViewCell
{
    UIActivityIndicatorView* loadingIndicator;
}
static NSOperationQueue* newQueue;
-(void) createWithStringURL: (NSString*) stringURL
{
    _contentViewer = [[UIImageView alloc] initWithFrame:self.bounds];
   // _contentViewer
//    NSURL *url = [NSURL URLWithString:stringURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
// 
//    [_contentViewer loadRequest:requestObj];
    
    //_contentViewer.delegate=self;
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];   
    _contentViewer.backgroundColor = [UIColor blackColor];
    _contentViewer.contentMode = UIViewContentModeScaleAspectFit;
     [self addSubview:_contentViewer];
     loadingIndicator.center = _contentViewer.center;
    [self addSubview:loadingIndicator];
    _descriptionLabel = [[UILabel alloc] init];
    _descriptionLabel.lineBreakMode =  NSLineBreakByWordWrapping ; 
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _descriptionLabel.textColor = [UIColor whiteColor];
    _descriptionLabel.text = _descriptionText;
   
    CGRect labelRect = [_descriptionText
                        boundingRectWithSize: CGSizeMake(self.bounds.size.width, 0)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName : _descriptionLabel.font
                                     }
                        context:nil];
    CGRect frame = CGRectMake(0, self.bounds.size.height-labelRect.size.height, self.bounds.size.width, labelRect.size.height);
     [_descriptionLabel setFrame:frame];
    [self addSubview:_descriptionLabel];
    
    if (!newQueue) newQueue= [[NSOperationQueue alloc] init];
    NSArray *operations = newQueue.operations;
    bool checkExistenceOperation = NO;
    for (DownloadOperation* tmp in operations) {
        if (tmp.imgView == self.contentViewer) {
           // [tmp cancel];
            checkExistenceOperation = YES;
            break;
        }
    }
     CacheManager* manager = [CacheManager sharedManager];
    UIImage* cachedImage = [manager loadFromCache:stringURL];
    if (!cachedImage){
    if (!checkExistenceOperation){
    DownloadOperation *newOperation = [[DownloadOperation alloc] init];
    newOperation.urlPath = stringURL;
    newOperation.imgView = self.contentViewer;
    newOperation.activityIndicator = loadingIndicator;
    [newQueue addOperation:newOperation];
    }
    }
    else
    {
        _contentViewer.image =cachedImage;
    }
   
}
-(void)prepareForReuse
{
    if (!newQueue) newQueue= [[NSOperationQueue alloc] init];
    NSArray *operations = newQueue.operations;
    for (DownloadOperation* tmp in operations) {
        if (tmp.imgView == self.contentViewer) {
            [tmp cancel];
            break;
        }
    }
    _contentViewer.image =nil;
    
}
- (void) dealloc
{
    NSArray *operations = newQueue.operations;
    for (DownloadOperation* tmp in operations) {
            [tmp cancel];
      
    }

}
@end
