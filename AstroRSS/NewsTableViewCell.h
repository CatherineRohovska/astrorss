//
//  NewsTableViewCell.h
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell
- (void) addText: (NSString*) text;
@end
