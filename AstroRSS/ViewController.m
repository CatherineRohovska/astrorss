//
//  ViewController.m
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "Feed.h"
#import "PresentationNewsViewController.h"
#import "BreakingNewsViewController.h"
#import "ImageOfTheDayViewController.h"
@interface ViewController ()

@end

@implementation ViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self createInterface];

    //  Feed* feed = [[Feed alloc] initWithURL:url];
    //[feed requestFinished];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) createInterface
{
    _breakingNewsButton.layer.borderWidth = 3;
    _breakingNewsButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [_breakingNewsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _breakingNewsButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    _educationNewsButton.layer.borderWidth = 3;
    _educationNewsButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    _educationNewsButton.titleLabel.textColor = [UIColor whiteColor];
    [_educationNewsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     _educationNewsButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    _imagesOfTheDayButton.layer.borderWidth = 3;
    _imagesOfTheDayButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    _imagesOfTheDayButton.titleLabel.textColor = [UIColor whiteColor];
    [_imagesOfTheDayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     _imagesOfTheDayButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
}
- (IBAction)breakingNewsButtonClick:(id)sender {
    BreakingNewsViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"breakingNewsViewController"];
    ctrl.networkAdress = @"http://www.nasa.gov/rss/dyn/breaking_news.rss";
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)educationNewsButtonClick:(id)sender {
    BreakingNewsViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"breakingNewsViewController"];
    ctrl.networkAdress = @"http://www.nasa.gov/rss/dyn/educationnews.rss";
    [self.navigationController pushViewController:ctrl animated:YES];

}
- (IBAction)imagesOfTheDayButtonClick:(id)sender {
    ImageOfTheDayViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"imageOfTheDayViewController"];
    ctrl.networkAdress = @"http://www.nasa.gov/rss/dyn/image_of_the_day.rss";
    [self.navigationController pushViewController:ctrl animated:YES];

}
@end
