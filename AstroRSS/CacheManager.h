//
//  CacheManager.h
//  AstroRSS
//
//  Created by User on 11/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CacheManager : NSObject
+ (id)sharedManager;
-(UIImage*) loadFromCache: (NSString*) resourceName;
-(BOOL) saveToCache: (NSString*) resourceName image: (UIImage*) image;

@end
