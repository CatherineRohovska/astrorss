//
//  Feed.h
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Channel.h"
#import "Post.h"
@interface Feed : NSObject <NSXMLParserDelegate>
@property (nonatomic) BOOL parsed;
@property (nonatomic, strong) NSURL *feedURL;
@property (nonatomic, strong) NSURLRequest *feedRequest;
@property (nonatomic, strong)  Channel *feedChannel;
@property (nonatomic, strong)  NSMutableArray *feedPosts;
-(id)initWithURL:(NSURL *)sourceURL;
- (void)requestFinished;
@end
