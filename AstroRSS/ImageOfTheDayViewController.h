//
//  ImageOfTheDayViewController.h
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageOfTheDayViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *imagePresentation;
@property (weak, nonatomic) IBOutlet UILabel *imageDescription;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) NSString* networkAdress;
- (IBAction)backButtonClick:(id)sender;

@end
