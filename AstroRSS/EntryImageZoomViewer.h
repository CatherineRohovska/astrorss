//
//  EntryImageZoomViewer.h
//  AstroRSS
//
//  Created by User on 11/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntryImageZoomViewer : UIScrollView
<UIScrollViewDelegate>
@property (strong, nonatomic) UIImage* image;
@end
