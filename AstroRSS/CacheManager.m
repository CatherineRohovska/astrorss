//
//  CacheManager.m
//  AstroRSS
//
//  Created by User on 11/12/15.
//  Copyright © 2015 User. All rights reserved.
//
#define MAX_SIZE_OF_CACHE 1024*1024*300lu;
#import "CacheManager.h"

@implementation CacheManager
{
    NSString *cacheDir;
    NSMutableArray *imgFiles;
    unsigned long long cacheSize;
}
+ (id)sharedManager {
    static CacheManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init ];
    });
    return sharedManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        //creating directory
        BOOL isDirectory;
        NSString *docDir = NSTemporaryDirectory();//[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        cacheDir = [docDir stringByAppendingPathComponent:@"CacheAstroRSS"];
        NSFileManager* manager = [NSFileManager defaultManager];
        if (![manager fileExistsAtPath:cacheDir isDirectory:&isDirectory] || !isDirectory) {
            NSError *error = nil;
            NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                             forKey:NSFileProtectionKey];
            [manager createDirectoryAtPath:cacheDir
               withIntermediateDirectories:YES
                                attributes:attr
                                     error:&error];
            if (error)
                NSLog(@"Error creating directory path: %@", [error localizedDescription]);
        }
        [self getCachedListOfFiles];
    }
    return self;
}
-(UIImage*) loadFromCache: (NSString*) resourceName
{
    UIImage* resource = nil;
   
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* resName = [[resourceName stringByReplacingOccurrencesOfString:@"/" withString:@""] stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString* filePath = [cacheDir stringByAppendingPathComponent:resName];
    if ([manager fileExistsAtPath:filePath isDirectory:NULL] )
    {
    resource= [UIImage imageWithContentsOfFile:filePath];
    }
    return resource;
}
-(BOOL) saveToCache: (NSString*) resourceName image: (UIImage*) image;
{
    BOOL result = NO;
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* resName = [[resourceName stringByReplacingOccurrencesOfString:@"/" withString:@""]stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString* filePath = [cacheDir stringByAppendingPathComponent:resName];
    NSData* imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, .8)];
    NSError *writeError = nil;
     BOOL isDirectory;
   if (![manager fileExistsAtPath:filePath isDirectory:&isDirectory] )
    {
    if(![imageData writeToFile:filePath options:NSDataWritingAtomic error:&writeError]) {
        NSLog(@"%@: Error saving image: %@", [self class], [writeError localizedDescription]);
    }
        else
        {
            //[imgFiles addObject:filePath];
            [self getCachedListOfFiles];
        }
    }
    
    return  result;
    
}
- (void) getCachedListOfFiles
{
    cacheSize = 0;
    NSArray* dirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cacheDir
                                                                        error:NULL];
    imgFiles = [[NSMutableArray alloc] init];

    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSError* error = nil;
        NSString *filename = (NSString *)obj;
        NSString *extension = [[filename pathExtension] lowercaseString];
        if ([extension isEqualToString:@"jpg"]) {
            [imgFiles addObject:[cacheDir stringByAppendingPathComponent:filename]];
            cacheSize = cacheSize+[[[NSFileManager defaultManager] attributesOfItemAtPath:[cacheDir stringByAppendingPathComponent:filename] error:&error] fileSize];
            if (error)
            {
                 NSLog(@"%@: Error : %@", [self class], [error localizedDescription]);
            }
        }
    }];
    unsigned long long maxSize = MAX_SIZE_OF_CACHE;
    if (cacheSize > maxSize) {
        for (NSString* filename in imgFiles) {
            NSDate *fileDate = [[[NSFileManager defaultManager] attributesOfItemAtPath:[cacheDir stringByAppendingPathComponent:filename] error:NULL] fileCreationDate];
            NSDate* nowDate =[NSDate date];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [calendar components:NSCalendarUnitDay
                                                       fromDate:fileDate
                                                         toDate:nowDate
                                                        options:0];
            if (components.day>10) {
                [[NSFileManager defaultManager] removeItemAtPath:[cacheDir stringByAppendingString:filename]  error:nil];
                
            }

        }
    // NSString* filename = [imgFiles objectAtIndex:0];
    
        
    }
}

@end
