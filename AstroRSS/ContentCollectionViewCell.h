//
//  ContentCollectionViewCell.h
//  AstroRSS
//
//  Created by User on 11/10/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UIImageView* contentViewer;
@property (strong, nonatomic) UILabel* descriptionLabel;
@property (strong, nonatomic) NSString* descriptionText;
-(void) createWithStringURL: (NSString*) stringURL;
@end
