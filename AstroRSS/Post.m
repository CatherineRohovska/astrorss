//
//  Post.m
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "Post.h"

@implementation Post
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"";
        self.descript =@"";
        self.guide = @"";
        self.enclosure = @"";
        self.pubDate = @"";
        self.imageLink = @"";
    }
    return self;
}
@end
