//
//  EducationNewsViewController.h
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface PresentationNewsViewController : UIViewController
<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *forwardLink;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)backButtonClick:(id)sender;
- (IBAction)linkClicked:(id)sender;
- (IBAction)sendButtonViaEmailButtonClicked:(id)sender;

@property (nonatomic, strong) NSString* descript;
@property (nonatomic, strong) NSString* imageLink;
@property (nonatomic, strong) NSString* fullNewsLink;
@end
