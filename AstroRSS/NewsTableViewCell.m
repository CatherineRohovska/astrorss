//
//  NewsTableViewCell.m
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "NewsTableViewCell.h"

@implementation NewsTableViewCell
{
    NSTimer* timer;
    NSString* fullText;
    NSInteger len;
    NSInteger positionToScroll;
    NSInteger maxLength;
}

- (void)awakeFromNib {
    // Initialization code


   // NSLog(@"%@", self.textLabel.text);
}
- (void) addText: (NSString*) text
{
    self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.textLabel.numberOfLines = 0;
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.text = text;
    
    fullText = [text copy];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
//    [timer invalidate];
//     self.textLabel.text = fullText;
//    // Configure the view for the selected state
//    if (!selected) {
//        [timer invalidate];
//        if (fullText) {
//            self.textLabel.text = fullText;
//        }
//       // self.textLabel.text = fullText;
//        
//    }
//    else
//    {
//        len=0;
//        positionToScroll =0;
//        maxLength = [self stringVisibleInRect:self.textLabel.frame withFont:self.textLabel.font]-3;
//      
//        timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
//                                                 target: self
//                                               selector:@selector(scrollText)
//                                               userInfo: nil
//                                                repeats:YES];
//    }
}
-(void)scrollText
{
 
    if (positionToScroll+len==fullText.length) {
        len=0;
        positionToScroll=0;
       
    }
    
    if (len>maxLength) {
        positionToScroll++;
         self.textLabel.text = [fullText substringWithRange:NSMakeRange(positionToScroll, len)];
    }
    else
    { len = maxLength;
        positionToScroll++;
       self.textLabel.text = [fullText substringWithRange:NSMakeRange(positionToScroll, len)];
    }
    
  
}
- (NSInteger)stringVisibleInRect:(CGRect)rect withFont:(UIFont*)font //to calculate visible count of letters
{
    NSString *visibleString = @"";
    for (int i = 1; i <= self.textLabel.text.length; i++)
    {
        NSString *testString = [self.textLabel.text substringToIndex:i];
        CGSize stringSize = [testString sizeWithAttributes:
                             @{NSFontAttributeName: self.textLabel.font}];
        if (stringSize.height > rect.size.height || stringSize.width > rect.size.width)
            break;
        
        visibleString = testString;
    }
    return visibleString.length;
}
@end
