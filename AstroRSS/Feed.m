//
//  Feed.m
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "Feed.h"
static NSString * const kChannelElementName = @"channel";
static NSString * const kItemElementName = @"item";
@implementation Feed
{
    NSData* responseData;
    Channel* currentChannel;
    Post* currentPost;
    id currentElement;
}
-(id)initWithURL:(NSURL *)sourceURL
{
    if (self = [super init]) {
        
        _feedURL = sourceURL;
        _feedPosts = [[NSMutableArray alloc] init];
        _feedRequest = [[NSURLRequest alloc] initWithURL:sourceURL];
        _parsed = NO;
      
        //
//        currentPost = [[Post alloc] init];
//        currentPost.title = [[NSString alloc] init];
//        currentPost.descript =[[NSString alloc] init];
//        currentPost.guide = [[NSString alloc] init];
//        currentPost.enclosure = [[NSString alloc] init];
        
    }
    
    return self;
}
-(void) showData
{
    
    NSLog(@"Request parsed");
    
}
- (void)requestFinished {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSURLSession* currentSession = [NSURLSession sharedSession];
        NSURLSessionTask* task = [currentSession dataTaskWithRequest:_feedRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            responseData = data;
            NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseData];
            parser.delegate = self;
            if ([parser parse]) {
                
                _parsed = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"ParsingComplete"
                     object:nil];
                });
            }
            [self showData];
        }];
        [task resume];
    });
  
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict {
    currentElement = [elementName copy];
    if ([elementName isEqualToString:kChannelElementName]) {
        
        Channel *channel = [[Channel alloc] init];
        channel.descript = [[NSString alloc] init];
        channel.link = [[NSString alloc] init];
        channel.title =[[NSString alloc] init];
        self.feedChannel = channel;
      currentChannel = channel;
     
       // return;
        
    }
    
    if ([elementName isEqualToString:kItemElementName]) {
        
       currentPost = [[Post alloc] init];
//        currentPost.title = [[NSString alloc] init];
//        currentPost.descript =[[NSString alloc] init];
//        currentPost.guide = [[NSString alloc] init];
//        currentPost.enclosure = [[NSString alloc] init];
          //   currentPost = post;
      //  return;
        
    }
    if([elementName isEqualToString:@"enclosure"])
    {
        NSString *urlValue=[attributeDict valueForKey:@"url"];
        currentPost.imageLink = urlValue;
      //  NSString *urlValue=[attributeDict valueForKey:@"type"];

    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    //NSLog(@"ended element: %@", elementName);
    if ([elementName isEqualToString:kItemElementName]) {
        // save values to an item, then store that item into the array...
     
     //   NSLog(@"adding story: %@", currentPost.descript);
        [_feedPosts addObject:currentPost];

   //     NSLog(@"Post: %@  Desr: %@ Link: %@", currentPost.title, currentPost.descript, currentPost.imageLink);
      
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //NSLog(@"found characters: %@", string);
    // save the characters for the current item...
   
    
    if ([currentElement isEqualToString:@"title"]) {
        currentPost.title =[currentPost.title stringByAppendingString: string];
    } else if ([currentElement isEqualToString:@"link"]) {
      currentPost.guide =[currentPost.guide stringByAppendingString: string];
    } else if ([currentElement isEqualToString:@"description"]) {
       currentPost.descript =[currentPost.descript stringByAppendingString: string];
    } else if ([currentElement isEqualToString:@"pubDate"]) {
       currentPost.pubDate =[currentPost.pubDate stringByAppendingString: string];
    }
  
   // NSLog(@"%@ %@", currentPost.title, currentPost.descript);
}

@end
