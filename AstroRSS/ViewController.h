//
//  ViewController.h
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *breakingNewsButton;
@property (weak, nonatomic) IBOutlet UIButton *educationNewsButton;
@property (weak, nonatomic) IBOutlet UIButton *imagesOfTheDayButton;
- (IBAction)breakingNewsButtonClick:(id)sender;
- (IBAction)educationNewsButtonClick:(id)sender;

- (IBAction)imagesOfTheDayButtonClick:(id)sender;


@end

