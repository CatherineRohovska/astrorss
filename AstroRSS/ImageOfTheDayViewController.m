//
//  ImageOfTheDayViewController.m
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ImageOfTheDayViewController.h"
#import "ContentCollectionViewCell.h"
#import "EntryImageZoomViewer.h"
#import "Feed.h"
@interface ImageOfTheDayViewController ()
{
Feed* feed;
}

@end

@implementation ImageOfTheDayViewController
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSURL* url = [[NSURL alloc] initWithString:_networkAdress];
    feed = [[Feed alloc] initWithURL:url];
    [feed requestFinished];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initiateData)
                                                 name:@"ParsingComplete"
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
//size of image cell
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[_imgColletion.collectionViewLayout invalidateLayout];
    //    double height = self.imgColletion.bounds.size.height;//self.view.bounds.size.height;
    CGSize size;
    size.width = _imagePresentation.bounds.size.width;
    size.height = _imagePresentation.bounds.size.height;
    
    return  size;   // return size;
}
//creating visible cell
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContentCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"presentationCell" forIndexPath:indexPath];
    Post* post;
   
       post  = [feed.feedPosts objectAtIndex:indexPath.item];
       cell.descriptionText = post.descript;
       [cell createWithStringURL:post.imageLink];
    
  
    return  cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    EntryImageZoomViewer* viewEntryPicture = [[EntryImageZoomViewer alloc] initWithFrame:self.view.bounds];
    
    ContentCollectionViewCell* cell = (ContentCollectionViewCell*)[collectionView cellForItemAtIndexPath: indexPath];
    UIImage* entryImage = cell.contentViewer.image;//[cell.contentViewer.image copy];
    viewEntryPicture.image = entryImage;
    [self.view addSubview:viewEntryPicture];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return feed.feedPosts.count;//counts data to show
}
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) initiateData
{
    [_imagePresentation reloadData];
}
@end
