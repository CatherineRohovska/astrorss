//
//  Channel.h
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Channel : NSObject
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* descript;
@property (nonatomic, strong) NSString* link;
@end
