//
//  AppDelegate.h
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong) UINavigationController* navigationController;

@end

