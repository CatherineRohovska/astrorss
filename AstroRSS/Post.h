//
//  Post.h
//  AstroRSS
//
//  Created by User on 11/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject
@property (nonatomic) BOOL isRead;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* descript;
@property (nonatomic, strong) NSString* guide;
@property (nonatomic, strong) NSString* enclosure;
@property (nonatomic, strong) NSString* pubDate;
@property (nonatomic, strong) NSString* imageLink;
@end
