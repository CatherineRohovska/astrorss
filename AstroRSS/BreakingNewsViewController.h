//
//  BreakingNewsViewController.h
//  AstroRSS
//
//  Created by User on 11/6/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreakingNewsViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *breakingNewsTable;
@property (strong, nonatomic) NSString* networkAdress;
- (IBAction)backButtonClicked:(id)sender;

@end
